import sys
import math
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication, QWidget, QMessageBox, QFileDialog, QTextEdit
from PyQt5.QtGui import QIcon
from PyQt5.QtOpenGL import *
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *


class OGLWidget(QGLWidget):
    '''
    Widget for drawing two spirals.
    '''

    def __init__(self, parent):
        QGLWidget.__init__(self, parent)
        self.setMinimumSize(500, 500)
        self.rotate = 0.0
        self.zoom = 0.0
        self.model = []
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(20)

    def set_obj(self, model):
        self.model = model

    def paintGL(self):
        '''
        Drawing routine
        '''

        glEnable(GL_DEPTH_TEST)
        glEnable(GL_BLEND)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glDisable(GL_CULL_FACE)
        glEnable(GL_LIGHTING)
        glEnable(GL_LIGHT0)
        glLoadIdentity()

        glTranslate(0.0, 0.0, self.zoom)
        glRotate(self.rotate, 0.2, 1.0, 0.7)

        if self.model:
            glBegin(GL_TRIANGLES)
            for face in self.model['f']:
                if face['vt']:
                    vt = self.model['vt'][face['vt']]
                    if vt:
                        glTexCoord(vt[0], vt[1])
                if face['vn']:
                    vn = self.model['vn'][face['vn']]
                    if vn:
                        glNormal(vn[0], vn[1], vn[2])

                v = self.model['v'][face['v']]
                glVertex3f(v[0], v[1], v[2])
            glEnd()

        self.rotate += 0.5
        self.zoom = -4.38

        glFlush()

    def resizeGL(self, w, h):
        '''
        Resize the GL window
        '''

        quadratic = gluNewQuadric()
        gluQuadricNormals(quadratic, GLU_SMOOTH)  # Create Smooth Normals
        gluQuadricTexture(quadratic, GL_TRUE)  # Create Texture Coords

        glViewport(0, 0, w, h)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, float(w) / float(h), 0.01, 500.0)
        glMatrixMode(GL_MODELVIEW)



    def initializeGL(self):
        '''
        Initialize GL
        '''

        # set viewing projection
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClearDepth(1.0)

        glDepthFunc(GL_LESS)
        glEnable(GL_DEPTH_TEST)
        glShadeModel(GL_SMOOTH)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(0, 0, 0, 0)
        glMatrixMode(GL_MODELVIEW)


class FullApp(QMainWindow):

    def __init__(self):
        super().__init__()
        self.obj_data = None
        self.ogl = None
        self.init_ui()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message',
                                           "Are you sure to quit?", QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
        event.accept()

    def init_ui(self):

        exitAction = QAction(QIcon('exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit Application')
        exitAction.triggered.connect(self.close)

        self.statusBar()

        openFile = QAction(QIcon('open.png'), 'Open', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open OBJ File')
        openFile.triggered.connect(self.showDialog)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')

        fileMenu.addAction(openFile)
        fileMenu.addAction(exitAction)

        self.setGeometry(300, 300, 300, 300)
        self.setWindowTitle('Test App')

        self.ogl = OGLWidget(self)
        self.setCentralWidget(self.ogl)


    def showDialog(self):

        fname = QFileDialog.getOpenFileName(self, 'Open File')
        if fname[0]:
            f = open(fname[0], 'r')

            with f:
                data = f.read()
                self.obj_data = data
                self.parse_model()

    def parse_model(self):
        model = {
            "v": [],
            "vt": [],
            "vn": [],
            "f": []
        }
        for line in iter(self.obj_data.splitlines()):
            # Vertex Reference
            if line.find("v ") == 0:
                vert = []
                for t in line.replace("v ", "").split(" "):
                    vert.append(float(t))
                model['v'].append(vert)

            elif line.find('vt ') == 0:
                vert = []
                for t in line.replace("vt ", "").split(" "):
                    vert.append(float(t))
                model['vt'].append(vert)

            elif line.find('vn ') == 0:
                vert = []
                for t in line.replace("vn ", "").split(" "):
                    vert.append(float(t))
                model['vn'].append(vert)

            elif line.find('f ') == 0:
                for face in line.replace("f ", "").split(" "):
                    poly_vert = face.split('/')
                    model['f'].append({
                        'v': int(poly_vert[0]) - 1 if poly_vert[0] else None,
                        'vt': int(poly_vert[1]) - 1 if poly_vert[1] else None,
                        'vn': int(poly_vert[2]) - 1 if poly_vert[2] else None
                    })

        self.ogl.set_obj(model)


def main():
    app = QApplication(sys.argv)
    ex = FullApp()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

